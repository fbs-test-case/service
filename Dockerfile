FROM golang:1.16.3-buster AS builder

WORKDIR /opt/build

COPY . .

RUN go test -race ./... && go build -o app ./cmd/main

FROM debian:buster-20210408

WORKDIR /opt/app

COPY --from=builder /opt/build/app ./app

ENTRYPOINT ["./app"]
