# Deployment
`docker-compose up --build`

# Monkey testing
## REST
`http :3333/fib/range x==0 y==100`

## gRPC
`evans -p 3334 api/fib.proto` => `call Range`
