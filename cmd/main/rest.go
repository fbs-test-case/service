package main

import (
	"errors"
	"net/http"

	"github.com/gofiber/fiber/v2"

	"fbs/internal/fib"
)

type RESTService struct {
	fibStore fib.Store
}

func (s *RESTService) Range(ctx *fiber.Ctx) error {
	req := struct {
		X int `query:"x"`
		Y int `query:"y"`
	}{}
	if err := ctx.QueryParser(&req); err != nil {
		return ctx.SendStatus(http.StatusBadRequest)
	}

	seq, err := s.fibStore.Range(req.X, req.Y)
	if err != nil {
		if errors.Is(err, fib.ErrValue) {
			return ctx.SendStatus(http.StatusBadRequest)
		}
		return ctx.SendStatus(http.StatusInternalServerError)
	}
	return ctx.JSON(seq)
}
