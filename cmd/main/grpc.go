package main

import (
	"context"
	"errors"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"fbs/internal/fib"
	pb "fbs/pkg/gitlab.com/fbs-test-case/api"
)

type GRPCService struct {
	pb.UnimplementedFibServiceServer
	fibStore fib.Store
}

func (s *GRPCService) Range(ctx context.Context, in *pb.FibRangeRequest) (*pb.FibRangeReply, error) {
	seq, err := s.fibStore.Range(int(in.X), int(in.Y))
	if err != nil {
		if errors.Is(err, fib.ErrValue) {
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}
		return nil, status.Error(codes.Internal, "")
	}

	r := make([]string, 0, len(seq))
	for _, val := range seq {
		r = append(r, val.String())
	}
	return &pb.FibRangeReply{Sequence: r}, nil
}
