package main

import (
	"log"
	"net"
	"os"
	"os/signal"
	"sync"
	"time"

	"github.com/gofiber/fiber/v2"
	recover "github.com/gofiber/fiber/v2/middleware/recover"
	"google.golang.org/grpc"

	"fbs/internal/fib"
	pb "fbs/pkg/gitlab.com/fbs-test-case/api"
)

func main() {
	shutdownWg := &sync.WaitGroup{}

	restServer := fiber.New(fiber.Config{
		ReadTimeout:  time.Second * 25,
		WriteTimeout: time.Second * 25,
	})
	restServer.Use(recover.New())
	shutdownWg.Add(1)

	grpcServer := grpc.NewServer()
	shutdownWg.Add(1)

	fibStore := fib.NewRAMStore()

	restService := &RESTService{fibStore}
	restServer.Get("/fib/range", restService.Range)

	grpcService := &GRPCService{fibStore: fibStore}
	pb.RegisterFibServiceServer(grpcServer, grpcService)

	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt)
	defer func() {
		<-interrupt
		go func() {
			if err := restServer.Shutdown(); err != nil {
				log.Printf("[ERROR] failed to gracefully shutdown fiber: %v", err)
			}
			shutdownWg.Done()
		}()

		go func() {
			grpcServer.GracefulStop()
			shutdownWg.Done()
		}()
	}()

	go func() {
		log.Fatalf("[FATAL] fiber can't serve: %v", restServer.Listen(":80"))
	}()

	go func() {
		lis, err := net.Listen("tcp", ":8080")
		if err != nil {
			log.Fatalf("[FATAL] failed to listen: %v", err)
		}
		log.Fatalf("[FATAL] grpc can't serve: %v", grpcServer.Serve(lis))
	}()

	shutdownWg.Wait()
}
