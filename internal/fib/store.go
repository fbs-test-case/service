package fib

import (
	"errors"
	"math/big"
)

type Store interface {
	// Range returns a fibonacci subsequence S | S = {Fib_x, ..., Fib_y}
	Range(x, y int) ([]*big.Int, error)
}

var (
	ErrValue = errors.New("value error")
)
