package fib

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestRAMStore_Range(t *testing.T) {
	type testCase struct {
		name string
		x    int
		y    int
		err  error
	}
	cases := []testCase{
		{"ok", 0, 99, nil},
		{"ok: 0 1", 0, 1, nil},
		{"ok: 1 10", 1, 10, nil},
		{"ok: 10 100", 10, 100, nil},
		{"ok: single item", 1, 1, nil},
		{"invalid range: 0 0", 0, 0, ErrValue},
		{"invalid range: -1 0", -1, 0, ErrValue},
		{"invalid range: 0 -1", 0, -1, ErrValue},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			fibStore := NewRAMStore()
			seq, err := fibStore.Range(tc.x, tc.y)
			if err != nil {
				assert.Nil(t, seq)
				assert.ErrorIs(t, err, tc.err)
				return
			}

			assert.NotNil(t, seq)
			for i, v := range seq {
				assert.Equal(t, fibSeq100[tc.x+i], v.Text(10))
			}
		})
	}
}

func TestRAMStore_RangePreCalc(t *testing.T) {
	fibStore := NewRAMStore()
	seq, err := fibStore.Range(50, 99)
	assert.Nil(t, err)
	assert.NotNil(t, seq)

	for i, v := range seq {
		assert.Equal(t, fibSeq100[50+i], v.Text(10))
	}
}

func TestRAMStore_RangeCacheValidness(t *testing.T) {
	fibStore := NewRAMStore()
	seq, err := fibStore.Range(0, 99)
	assert.Nil(t, err)
	assert.NotNil(t, seq)

	seq1, err := fibStore.Range(50, 99)
	assert.Nil(t, err)
	assert.NotNil(t, seq1)

	for i, v := range seq1 {
		assert.Equal(t, fibSeq100[50+i], v.Text(10))
	}
}

func BenchmarkRAMStore_Range(b *testing.B) {
	fibStore := NewRAMStore()
	seq, err := fibStore.Range(0, 1<<16)
	assert.Nil(b, err)
	assert.NotNil(b, seq)
}

var fibSeq100 = []string{
	"0",
	"1",
	"1",
	"2",
	"3",
	"5",
	"8",
	"13",
	"21",
	"34",
	"55",
	"89",
	"144",
	"233",
	"377",
	"610",
	"987",
	"1597",
	"2584",
	"4181",
	"6765",
	"10946",
	"17711",
	"28657",
	"46368",
	"75025",
	"121393",
	"196418",
	"317811",
	"514229",
	"832040",
	"1346269",
	"2178309",
	"3524578",
	"5702887",
	"9227465",
	"14930352",
	"24157817",
	"39088169",
	"63245986",
	"102334155",
	"165580141",
	"267914296",
	"433494437",
	"701408733",
	"1134903170",
	"1836311903",
	"2971215073",
	"4807526976",
	"7778742049",
	"12586269025",
	"20365011074",
	"32951280099",
	"53316291173",
	"86267571272",
	"139583862445",
	"225851433717",
	"365435296162",
	"591286729879",
	"956722026041",
	"1548008755920",
	"2504730781961",
	"4052739537881",
	"6557470319842",
	"10610209857723",
	"17167680177565",
	"27777890035288",
	"44945570212853",
	"72723460248141",
	"117669030460994",
	"190392490709135",
	"308061521170129",
	"498454011879264",
	"806515533049393",
	"1304969544928657",
	"2111485077978050",
	"3416454622906707",
	"5527939700884757",
	"8944394323791464",
	"14472334024676221",
	"23416728348467685",
	"37889062373143906",
	"61305790721611591",
	"99194853094755497",
	"160500643816367088",
	"259695496911122585",
	"420196140727489673",
	"679891637638612258",
	"1100087778366101931",
	"1779979416004714189",
	"2880067194370816120",
	"4660046610375530309",
	"7540113804746346429",
	"12200160415121876738",
	"19740274219868223167",
	"31940434634990099905",
	"51680708854858323072",
	"83621143489848422977",
	"135301852344706746049",
	"218922995834555169026",
	"354224848179261915075",
}
