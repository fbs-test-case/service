package fib

import (
	"fmt"
	"math/big"
	"sync"
)

type RAMStore struct {
	cache []*big.Int
	mtx   sync.RWMutex
}

func NewRAMStore() *RAMStore {
	return &RAMStore{
		cache: []*big.Int{big.NewInt(0), big.NewInt(1)},
		mtx:   sync.RWMutex{},
	}
}

func (s *RAMStore) Range(x, y int) ([]*big.Int, error) {
	if y < 0 || x < 0 {
		return nil, fmt.Errorf("%w: invalid range: x=%v, y=%v", ErrValue, x, y)
	}

	y++ // range must be inclusive

	count := y - x
	s.mtx.RLock()
	if x+count < len(s.cache) {
		defer s.mtx.RUnlock()
		return s.cache[x:y], nil
	}
	s.mtx.RUnlock()

	s.mtx.Lock()
	defer s.mtx.Unlock()
	workload := count
	if len(s.cache) < x {
		workload += x
	}
	if len(s.cache) > x {
		workload -= len(s.cache[x:])
	}
	tail := make([]*big.Int, 0, workload)
	left, right := s.cache[len(s.cache)-2], s.cache[len(s.cache)-1]
	for i := 0; i < workload; i++ {
		next := new(big.Int).Add(left, right)
		left = right
		right = next
		tail = append(tail, next)
	}
	s.cache = append(s.cache, tail...)
	return s.cache[x:y], nil
}
